package oop.classes.packageclass;

import oop.classes.packageclass.PackageClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PackageClassTest {
    
    @Test
    public void testIsObjectInstanceOfPackageClass() {
        final PackageClass obj = new PackageClass();
        final int mdf = PackageClass.class.getModifiers();

        assertEquals("Class is not in rigth package", "oop.classes.packageclass", obj.getClass().getPackage().getName());
        assertTrue("Class is not package access", mdf == 0);
    }
}
