import oop.classes.PublicClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class PublicClassTest {

    @Test
    public void testIsObjectInstanceOfPublicClass () {
        final int mdf = PublicClass.class.getModifiers();

        assertTrue("Class is not public access", mdf == 1);
    }
}
