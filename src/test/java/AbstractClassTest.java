import oop.classes.AbstractClass;
import org.junit.Test;

import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class AbstractClassTest
{

    @Test
    public void testIsObjectInstanceOfAbstractClass () {
        Class<AbstractClass> clazz = AbstractClass.class;

        assertTrue("Class is not abstract", Modifier.isAbstract(clazz.getModifiers()));
    }
}
