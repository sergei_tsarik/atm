import oop.classes.ObjectModelClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class ObjectModelClassTest {

    @Test
    public void testIsObjectInstanceOfClass () {
        final ObjectModelClass obj = new ObjectModelClass();

        assertTrue(obj instanceof ObjectModelClass);
    }
}
